//
//  UserProfileStrageServiceProtocol.swift
//  OpenPay_test
//
//  Created by Andrew Danishevskyi on 11/5/18.
//  Copyright © 2018 danishevskyi. All rights reserved.
//

import Foundation
protocol UserProfileStrageServiceProtocol {
    var userProfile: Profile? {get}
    func initStorage(completion: @escaping () -> Swift.Void)
    func cacheUserProfile(_ profile: Profile, completion: @escaping () -> Swift.Void)
}
