//
//  TAASREquestSender.swift
//  TAAS-test
//
//  Created by Andrew Danishevskyi on 09.10.2018.
//  Copyright © 2018 Andrew Danishevskyi. All rights reserved.
//

import Foundation
//class TAASREquestSender: RequestSender{
//    enum RquestSource: String{
//        case coinmarketcap = "https://coinmarketcap.com"
//        case cryptocompare = "https://cryptocompare.com"
//    }
//    let source: RquestSource
//    init(source: RquestSource){
//        self.source = source
//        switch source {
//        case .coinmarketcap:
//            super.init(baseURL: "https://api.coinmarketcap.com", resourcePath: "v2/ticker/?convert=BTC&limit=1", method: .GET, reachabilityManager: ReachabilityManager.shared)
//        case .cryptocompare:
//            super.init(baseURL: "https://min-api.cryptocompare.com", resourcePath: "data/pricemultifull?fsyms=BTC&tsyms=USD", method: .POST, reachabilityManager: ReachabilityManager.shared)
//        }
//    }
//    
//    func sendTAASRequest(completion: @escaping (PriceEntityProtocol) -> Swift.Void){
//        super.sendRequest(withSuccess: { (response) in
//            let price = PriceEntity(source: self.source.rawValue, value: self.getPrice(response: response))
//            completion(price)
//        }) { (_) in
//            completion(PriceEntity(source: self.source.rawValue, value: nil))
//        }
//    }
//    
//    private func getPrice(response: ResponseProtocol) -> Double?{
//        if let JSON = response.JSONBody(){
//            switch self.source{
//            case .coinmarketcap:
//                guard let data = JSON["data"] as? [String: Any], let subdata = data["1"] as? [String: Any], let quotes = subdata["quotes"] as? [String: Any], let usd = quotes["USD"] as? [String: Any] else {return nil}
//                return usd["price"] as? Double
//            case .cryptocompare:
//                guard let raw = JSON["RAW"] as? [String: Any], let btc = raw["BTC"] as? [String: Any], let usd = btc["USD"] as? [String: Any] else {return nil}
//                return usd["PRICE"] as? Double
//            }
//        }
//        return nil
//    }
//}
