//
//  CollectionViewCell.swift
//  OpenPay_test
//
//  Created by Andrew Danishevskyi on 11/13/18.
//  Copyright © 2018 danishevskyi. All rights reserved.
//

import UIKit

class UserCardCollectionViewCell: UICollectionViewCell {
    var imageView: UIImageView!
    override init(frame: CGRect){
        super.init(frame: frame)
        self.addImageView()
    }
    
    public required init?(coder aDecoder: NSCoder){
        super.init(coder: aDecoder)
        self.addImageView()
    }
    
    private func addImageView(){
        let imageView = UIImageView(frame: self.contentView.bounds)
        self.imageView = imageView
        self.contentView.backgroundColor = #colorLiteral(red: 0.748953104, green: 0.7490622401, blue: 0.7489292026, alpha: 1)
        self.contentView.addSubview(imageView)
        self.contentView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            imageView.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor),
            imageView.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor),
            imageView.topAnchor.constraint(equalTo: self.contentView.topAnchor),
            imageView.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor)
            ])
        self.contentView.layer.cornerRadius = 15
    }
}

extension UserCardCollectionViewCell: UserCardRepresentationViewProtocol{
    func setCardImage(url: URL?){
        self.imageView.setImageURL(url)
    }
}
