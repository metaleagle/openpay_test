//
//  CardsListViewProtocol.swift
//  OpenPay_test
//
//  Created by Andrew Danishevskyi on 11/12/18.
//  Copyright © 2018 danishevskyi. All rights reserved.
//

import Foundation
protocol CardsListViewProtocol {
    func updateLayout()
    func reloadCards()
}
