//
//  CardsListModuleInteractor.swift
//  OpenPay_test
//
//  Created by Andrew Danishevskyi on 11/13/18.
//  Copyright © 2018 danishevskyi. All rights reserved.
//

import Foundation
class CardsListModuleInteractor{
    let getUserCardsService: GetUserCardsServiceProtocol
    let userCardsStorage: UserCardsStorageServiceProtocol
    init(getUserCardsService: GetUserCardsServiceProtocol, userCardsStorage: UserCardsStorageServiceProtocol){
        self.getUserCardsService = getUserCardsService
        self.userCardsStorage = userCardsStorage
    }
}

extension CardsListModuleInteractor: CardsListModuleInteractorProtocol{
    func getUserCards(completion: @escaping () -> Swift.Void, failure: @escaping (String) -> Swift.Void){
        if nil != self.userCardsStorage.userCards{
            completion()
        }
        self.getUserCardsService.loadUserCardsList(complition: { (userCards) in
            self.userCardsStorage.cacheUserCards(userCards, completion: {
                completion()
            })
        }) { (errorMessage) in
            failure(errorMessage)
        }
    }
    
    var cardsNumber: Int {
        return self.userCardsStorage.userCards?.count ?? 0
    }
    
    func userCard(at index: Int) -> UserCard?{
        if index < self.cardsNumber{
            return self.userCardsStorage.userCards?[index]
        }
        return nil
    }
}
