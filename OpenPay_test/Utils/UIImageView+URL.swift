//
//  UIImageView+URL.swift
//  OpenPay_test
//
//  Created by Andrew Danishevskyi on 11/6/18.
//  Copyright © 2018 danishevskyi. All rights reserved.
//

import UIKit
extension UIImageView{
    func setImageURL(_ url: URL?, placeholderImage: UIImage? = nil){
        //here we can use images caching, e.g. KingFisher
        if let url = url{
            DispatchQueue.global(qos: .userInitiated).async{
                do{
                    let imageData = try Data(contentsOf: url)
                    let image = UIImage(data: imageData)
                    DispatchQueue.main.async {
                        self.image = image
                    }
                }
                catch{
                    self.image = placeholderImage
                }
                
            }
        }
        else{
            DispatchQueue.main.async {
                self.image = placeholderImage
            }
        }
    }
}
