//
//  ProfileModuleInteractorProtocol.swift
//  OpenPay_test
//
//  Created by Andrew Danishevskyi on 11/5/18.
//  Copyright © 2018 danishevskyi. All rights reserved.
//

import Foundation
protocol ProfileModuleInteractorProtocol {
    func initialInit(completion: @escaping () -> Swift.Void)
    func getUserProfile(completion: @escaping (Profile) -> Swift.Void, failure: @escaping (String) -> Swift.Void)
}
