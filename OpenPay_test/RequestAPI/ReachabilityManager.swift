//
//  ReachabilityManager.swift
//  KMM
//
//  Created by Andrew Danishevskyi on 25.05.17.
//  Copyright © 2017 Andrew Danishevskyi. All rights reserved.
//
import Foundation
protocol ReachabilityManagerProtocol{
    var isNetworkAvailable: Bool {get}
}

public protocol NetworkStatusListener : class {
    func networkStatusDidChange(status: Reachability.NetworkStatus)
}

class ReachabilityManager: ReachabilityManagerProtocol{
    static  let shared = ReachabilityManager()
    
    var isNetworkAvailable : Bool {
        return reachability.currentReachabilityStatus != .notReachable
    }
    private let reachability = Reachability()!
    private var listeners = [NetworkStatusListener]()
    
    @objc func reachabilityChanged(notification: Notification) {
        let reachability = notification.object as! Reachability
//        switch reachability.currentReachabilityStatus {
//        case .notReachable:
//            break
//        case .reachableViaWiFi:
//            break
//        case .reachableViaWWAN:
//            break
//        }
        
        for listener in listeners {
            listener.networkStatusDidChange(status: reachability.currentReachabilityStatus)
        }
    }
    
    func startMonitoring() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(reachabilityChanged(notification:)),
                                               name: ReachabilityChangedNotification,
                                               object: reachability)
        do{
            try reachability.startNotifier()
        } catch {

        }
    }
    
    func stopMonitoring(){
        reachability.stopNotifier()
        NotificationCenter.default.removeObserver(self,
                                                  name: ReachabilityChangedNotification,
                                                  object: reachability)
    }
    
    func addListener(listener: NetworkStatusListener){
        listeners.append(listener)
    }
    
    /// Removes a listener from listeners array
    ///
    /// - parameter delegate: the listener which is to be removed
    func removeListener(listener: NetworkStatusListener){
        listeners = listeners.filter{ $0 !== listener}
    }
    
}
