//
//  ProfileModuleAssembler.swift
//  OpenPay_test
//
//  Created by Andrew Danishevskyi on 11/5/18.
//  Copyright © 2018 danishevskyi. All rights reserved.
//

import UIKit
class ProfileModuleAssembler{
    class func profileModuleView() -> UIViewController{
        let cardsListView = CardsListModuleAssembler.cardsListModule()
        let view = ProfileModuleView(cardsListView: cardsListView)
        let presenter = ProfileModuleViewPresenter()
        view.presenter = presenter
        
        let getProfileRequestSender = OPRequestSender(type: .profile)
        let getUserInfoService = GetUserInfoService(requestSender: getProfileRequestSender)
        let userProfileStrageService = UserProfileStrageService()
        let interactor = ProfileModuleInteractor(getUserInfoService: getUserInfoService, userProfileStorage: userProfileStrageService)
        presenter.interactor = interactor
        
        let swipeInteractionController = SwipeInteractionController()
        let router = ProfileModuleRouter(transitioningDelegate: swipeInteractionController)
        presenter.router = router
        
        return view
    }
}
