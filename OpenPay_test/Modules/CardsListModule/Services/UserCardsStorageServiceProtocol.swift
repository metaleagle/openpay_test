//
//  UserCardsStorageProtocol.swift
//  OpenPay_test
//
//  Created by Andrew Danishevskyi on 11/13/18.
//  Copyright © 2018 danishevskyi. All rights reserved.
//

import Foundation
protocol UserCardsStorageServiceProtocol{
    var userCards: [UserCard]? {get}
    func cacheUserCards(_ cards: [UserCard], completion: @escaping () -> Swift.Void)
}
