//
//  SettingsModuleView.swift
//  OpenPay_test
//
//  Created by Andrew Danishevskyi on 11/12/18.
//  Copyright © 2018 danishevskyi. All rights reserved.
//

import UIKit
class SettingsModuleView: UIViewController{
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Settings"
        if self == self.navigationController?.viewControllers.first{
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "navBarBack").withRenderingMode(.alwaysTemplate), style: .plain, target: self, action: #selector(quit))
        }
        self.view.frame = UIScreen.main.bounds
        self.view.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    }
    
    @objc fileprivate func quit(){
        self.dismiss(animated: true, completion: nil)
    }
    
}
