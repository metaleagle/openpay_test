//
//  CardsListModulePresenterProtocol.swift
//  OpenPay_test
//
//  Created by Andrew Danishevskyi on 11/13/18.
//  Copyright © 2018 danishevskyi. All rights reserved.
//

import Foundation
protocol CardsListModulePresenterProtocol{
    func getUserCards(completion: @escaping()->Swift.Void, failure: @escaping (String)->Swift.Void)
    var cardsNumber: Int {get}
    func setup(cardView: UserCardRepresentationViewProtocol, at index: Int)
}
