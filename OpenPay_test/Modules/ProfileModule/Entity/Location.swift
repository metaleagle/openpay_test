//
//  Location.swift
//  OpenPay_test
//
//  Created by Andrew Danishevskyi on 11/5/18.
//  Copyright © 2018 danishevskyi. All rights reserved.
//

import Foundation
struct Location{
    let country: String?
    let state: String?
    let city: String?
    
    init(country: String?, state: String?, city: String?){
        self.country = country
        self.state = state
        self.city = city
    }
    
    init?(with JSON: [String: Any]?){
        guard let JSON = JSON else {return nil}
        self.init(country: JSON["country"] as? String, state: JSON["state"] as? String, city: JSON["city"] as? String)
    }
}
