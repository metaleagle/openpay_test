//
//  OPRequestSender.swift
//  OpenPay_test
//
//  Created by Andrew Danishevskyi on 11/3/18.
//  Copyright © 2018 danishevskyi. All rights reserved.
//

import Foundation
private let kServerURL = "https://s3-ap-southeast-2.amazonaws.com"
class OPRequestSender: RequestSender{
    enum RequestType: String{
        case profile = "openpay-mobile-test/profile.json"
        case cardsList = "openpay-mobile-test/cards.json"
    }
    
    let type: RequestType
    
    init(type: RequestType){
        self.type = type
        super.init(baseURL: kServerURL, resourcePath: type.rawValue, method: .GET, reachabilityManager: ReachabilityManager.shared)
    }
}
