//
//  UserProfileStrageService.swift
//  OpenPay_test
//
//  Created by Andrew Danishevskyi on 11/5/18.
//  Copyright © 2018 danishevskyi. All rights reserved.
//

import Foundation
class UserProfileStrageService{
    fileprivate var profile: Profile?
}

extension UserProfileStrageService: UserProfileStrageServiceProtocol{
    func initStorage(completion: @escaping () -> Swift.Void){
        //initialize data storage, e.g. CoreData, wich may take some time
        completion()
    }
    
    var userProfile: Profile?{
        //extract saved profile
        return profile
    }
    
    func cacheUserProfile(_ profile: Profile, completion: @escaping () -> Void) {
        //save profile to data storage
        self.profile = profile
    }
}
