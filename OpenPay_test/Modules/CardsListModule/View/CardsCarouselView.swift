//
//  CardsCarouselView.swift
//  ScrollViewTest
//
//  Created by Andrew Danishevskyi on 11/12/18.
//  Copyright © 2018 danishevskyi. All rights reserved.
//

import UIKit

class CardsCarouselView: UICollectionView {
    
    var presenter: CardsListModulePresenterProtocol?

    let cellWidthScaling: CGFloat
    let verticalInset: CGFloat
    
    init(cellScaling: CGFloat, verticalInset: CGFloat, frame: CGRect = CGRect.zero){
        self.cellWidthScaling = cellScaling
        self.verticalInset = verticalInset
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        flowLayout.minimumLineSpacing = 25
        super.init(frame: frame, collectionViewLayout: flowLayout)
        self.dataSource = self
        self.delegate = self
        self.register(UserCardCollectionViewCell.self, forCellWithReuseIdentifier: "CardCollectionViewCell")
        self.backgroundColor = #colorLiteral(red: 0.8833663464, green: 0.8976573348, blue: 0.9522421956, alpha: 1)
        self.translatesAutoresizingMaskIntoConstraints = false
    }
    
    public required init?(coder aDecoder: NSCoder){
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func updateFlowLayout(){
        if let containerView = self.superview{
            let cellWidth = floor(containerView.frame.size.width * cellWidthScaling)
            let cellHeight = floor(self.frame.size.height - (2 * verticalInset))
            let insetX = (containerView.frame.size.width - cellWidth) / 2.0

            let layout = self.collectionViewLayout as! UICollectionViewFlowLayout
            layout.itemSize = CGSize(width: cellWidth, height: cellHeight)
            self.contentInset = UIEdgeInsets(top: verticalInset, left: insetX, bottom: verticalInset, right: insetX)
        }
    }

}

extension CardsCarouselView: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        let cardsNumber = self.presenter?.cardsNumber ?? 0
        return cardsNumber
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        let cell = self.dequeueReusableCell(withReuseIdentifier: "CardCollectionViewCell", for: indexPath) as! UserCardCollectionViewCell
        self.presenter?.setup(cardView: cell, at: indexPath.row)
        return cell
    }

    public func numberOfSections(in collectionView: UICollectionView) -> Int{
        return 1
    }
}

extension CardsCarouselView: UIScrollViewDelegate, UICollectionViewDelegate{
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let layout = self.collectionViewLayout as! UICollectionViewFlowLayout
        let cellWidthIncludingSpaces = layout.itemSize.width + layout.minimumLineSpacing
        var offset = targetContentOffset.pointee
        let index = (offset.x + scrollView.contentInset.left) / cellWidthIncludingSpaces
        let roundedIndex = round(index)
        offset = CGPoint(x: roundedIndex * cellWidthIncludingSpaces - self.contentInset.left, y: -self.contentInset.top)
        targetContentOffset.pointee = offset
    }
}

extension CardsCarouselView: CardsListViewProtocol{
    func updateLayout(){
        self.updateFlowLayout()
    }
    
    func reloadCards(){
        self.presenter?.getUserCards(completion: {
            DispatchQueue.main.async {
                self.reloadData()
            }
        }, failure: { (errorMesage) in
            
        })
    }
}
