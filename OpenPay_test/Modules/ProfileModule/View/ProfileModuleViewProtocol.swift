//
//  ProfileModuleViewProtocol.swift
//  OpenPay_test
//
//  Created by Andrew Danishevskyi on 11/5/18.
//  Copyright © 2018 danishevskyi. All rights reserved.
//

import Foundation
protocol ProfileModuleViewProtocol: class {
    func showUer(name: String?)
    func showUser(location: String?)
    func setUserIcon(url: URL?)
}
