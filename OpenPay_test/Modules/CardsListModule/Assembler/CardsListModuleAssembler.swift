//
//  CardsListModuleAssembler.swift
//  OpenPay_test
//
//  Created by Andrew Danishevskyi on 11/12/18.
//  Copyright © 2018 danishevskyi. All rights reserved.
//

import Foundation
class CardsListModuleAssembler{
    class func cardsListModule() -> CardsListViewProtocol{
        let view = CardsCarouselView(cellScaling: 0.68, verticalInset: 34.0)
        
        let presenter = CardsListModulePresenter()
        view.presenter = presenter
        
        let getCardsService = GetUserCardsService(requestSender: OPRequestSender(type: .cardsList))
        let userCardsStorage = UserCardsStorageService()
        let interactor = CardsListModuleInteractor(getUserCardsService: getCardsService, userCardsStorage: userCardsStorage)
        presenter.interactor = interactor
        
        return view
    }
}
