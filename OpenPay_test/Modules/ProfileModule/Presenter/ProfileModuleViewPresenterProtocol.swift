//
//  ProfileModuleViewPresenterProtocol.swift
//  OpenPay_test
//
//  Created by Andrew Danishevskyi on 11/5/18.
//  Copyright © 2018 danishevskyi. All rights reserved.
//

import Foundation
protocol ProfileModuleViewPresenterProtocol: class {
    func initialInit(completion: @escaping () -> Swift.Void)
    func setup(view: ProfileModuleViewProtocol?)
    func showSettings<T>(from view: T)
}
