//
//  ProfileModuleViewPresenter.swift
//  OpenPay_test
//
//  Created by Andrew Danishevskyi on 11/5/18.
//  Copyright © 2018 danishevskyi. All rights reserved.
//

import Foundation
class ProfileModuleViewPresenter{
    var interactor: ProfileModuleInteractorProtocol?
    var router: ProfileModuleRouterProtocol?
}

extension ProfileModuleViewPresenter: ProfileModuleViewPresenterProtocol{
    func initialInit(completion: @escaping () -> Swift.Void){
        self.interactor?.initialInit(completion: completion)
    }
    
    func setup(view: ProfileModuleViewProtocol?){
        self.interactor?.getUserProfile(completion: {[weak view] (profile) in
            let userName = [profile.firstName, profile.lastName].joined(separator: " ")
            view?.showUer(name: userName)
            let location = [profile.location?.city, profile.location?.country].compactMap{$0}.joined(separator: ", ")
            view?.showUser(location: location)
            view?.setUserIcon(url:profile.avatar?.imageURL)
        }, failure: { (errorMessage) in
            
        })
    }
    
    func showSettings<T>(from view: T){
        self.router?.showSettings(from: view)
    }
}
