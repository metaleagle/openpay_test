//
//  Profile.swift
//  OpenPay_test
//
//  Created by Andrew Danishevskyi on 11/5/18.
//  Copyright © 2018 danishevskyi. All rights reserved.
//

import Foundation
struct Profile{
    let firstName: String
    let lastName: String
    let location: Location?
    let avatar: Avatar?
    
    init(firstName: String, lastName: String, location: Location?, avatar: Avatar?){
        self.firstName = firstName
        self.lastName = lastName
        self.location = location
        self.avatar = avatar
    }
    
    init?(with JSON: [String: Any]?){
        guard let JSON = JSON else {return nil}
        guard let firstName = JSON["firstName"] as? String, let lastName = JSON["lastName"] as? String else {return nil}
        self.init(firstName: firstName, lastName: lastName, location: Location(with: JSON["location"] as? [String: Any]), avatar: Avatar(with: JSON["avatar"] as? [String: Any]))
    }
}
