//
//  GetUserInfoService.swift
//  OpenPay_test
//
//  Created by Andrew Danishevskyi on 11/5/18.
//  Copyright © 2018 danishevskyi. All rights reserved.
//

import Foundation
class GetUserInfoService{
    fileprivate let requestSender: RequestSenderProtocol
    init(requestSender: RequestSenderProtocol){
        self.requestSender = requestSender
    }
}

extension GetUserInfoService: GetUserInfoServiceProtocol{
    func loadUserProfile(complition: @escaping (Profile) -> Void, failure: @escaping (String) -> Void) {
        self.requestSender.sendRequest(withSuccess: { (response) in
            if let profile = Profile(with: response.JSONBody()){
                complition(profile)
            }
            else{
                failure("Profile wasn't loaded - unknown response")
            }
        }) { (errorMessage) in
            failure(errorMessage)
        }
    }
}
