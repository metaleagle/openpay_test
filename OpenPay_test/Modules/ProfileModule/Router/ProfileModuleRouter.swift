//
//  ProfileModuleRouter.swift
//  OpenPay_test
//
//  Created by Andrew Danishevskyi on 11/5/18.
//  Copyright © 2018 danishevskyi. All rights reserved.
//

import UIKit
class ProfileModuleRouter{
    let transitioningDelegate: UIViewControllerTransitioningDelegate
    init(transitioningDelegate: UIViewControllerTransitioningDelegate){
        self.transitioningDelegate = transitioningDelegate
    }
}

extension ProfileModuleRouter: ProfileModuleRouterProtocol{
    func showSettings<T>(from view:T){
        if let view = view as? UIViewController{
            let settingsView = SettingsModuleAssembler.settingsModuleView()
            settingsView.transitioningDelegate = self.transitioningDelegate
            view.present(settingsView, animated: true, completion: nil)
        }
    }
}
