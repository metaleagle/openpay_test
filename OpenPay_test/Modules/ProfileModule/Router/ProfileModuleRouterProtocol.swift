//
//  ProfileModuleRouterProtocol.swift
//  OpenPay_test
//
//  Created by Andrew Danishevskyi on 11/5/18.
//  Copyright © 2018 danishevskyi. All rights reserved.
//

import Foundation
protocol ProfileModuleRouterProtocol {
    func showSettings<T>(from view:T)
}
