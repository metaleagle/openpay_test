//
//  log.swift
//  CipherME
//
//  Created by Andrew Danishevskyi on 17.09.2018.
//  Copyright © 2018 Andrew Danishevskyi. All rights reserved.
//

import Foundation
class log{
    class func debug(_ logMessage: String){
        #if DEBUG
        NSLog(logMessage)
        #endif
    }
}
