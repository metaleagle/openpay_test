//
//  Avatar.swift
//  OpenPay_test
//
//  Created by Andrew Danishevskyi on 11/5/18.
//  Copyright © 2018 danishevskyi. All rights reserved.
//

import Foundation
struct Avatar{
    let imageURLString: String
    let width: Int?
    let height: Int?
    var imageURL: URL?{
        return URL(string: self.imageURLString)
    }
    
    init(imageURLString: String, width: Int?, height: Int?){
        self.imageURLString = imageURLString
        self.width = width
        self.height = height
    }
    
    init?(with JSON: [String: Any]?){
        guard let JSON = JSON else {return nil}
        guard let urlString = JSON["imageUrl"] as? String else {return nil}
        self.init(imageURLString: urlString, width: JSON["width"] as? Int, height: JSON["height"] as? Int)
    }
}
