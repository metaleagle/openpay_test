//
//  TransitionAnimation.swift
//  KMM
//
//  Created by Андрей Данишевский on 26.05.17.
//  Copyright © 2017 danishevskyi. All rights reserved.
//

import UIKit

class PushTransitionAnimation:NSObject, UIViewControllerAnimatedTransitioning {
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.3
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        
        guard let fromViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from),
            let toViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to) else {
                return
        }
        let containerView = transitionContext.containerView
        let snapshot: UIView = toViewController.view.snapshotView(afterScreenUpdates: true) ?? toViewController.view
        snapshot.transform = CGAffineTransform.init(translationX: snapshot.frame.size.width, y:0)
        toViewController.view.isHidden = true
        containerView.addSubview(toViewController.view)
        containerView.addSubview(snapshot)

        UIView.animate(withDuration: transitionDuration(using: transitionContext), animations: {
            fromViewController.view.alpha = 0.8
            snapshot.transform = CGAffineTransform.identity
        }) { (complited) in
            toViewController.view.isHidden = false
            fromViewController.view.alpha = 1.0
            snapshot.removeFromSuperview()
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        }
        
        
    }
}

class PopTransitionAnimation:NSObject, UIViewControllerAnimatedTransitioning {
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.3
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        
        guard let fromViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from),
            let toViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to) else {
                return
        }
        let containerView = transitionContext.containerView
        let snapshot: UIView = fromViewController.view.snapshotView(afterScreenUpdates: true) ?? fromViewController.view
        
        toViewController.view.alpha = 0.8
        containerView.addSubview(toViewController.view)
        containerView.addSubview(snapshot)
        fromViewController.view.isHidden = true
        
        UIView.animate(withDuration: transitionDuration(using: transitionContext), animations: {
            toViewController.view.alpha = 1.0
            snapshot.transform = CGAffineTransform.init(translationX: snapshot.frame.size.width, y:0)
        }) { (complited) in
            if (!complited){
                toViewController.view.alpha = 1.0
            }
            toViewController.view.removeFromSuperview()
            fromViewController.view.isHidden = false
            snapshot.removeFromSuperview()
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        }
        
        
    }
}

class SwipeInteractionController: UIPercentDrivenInteractiveTransition{
    var interactionInProgress = false
    private var shouldCompleteTransition = false
    private weak var viewController: UIViewController!
    
    func wireToViewController(viewController: UIViewController!) {
        self.viewController = viewController
        prepareGestureRecognizerInView(view: viewController.view)
    }
    
    private func prepareGestureRecognizerInView(view: UIView) {
        let gesture = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(handleGesture(gestureRecognizer:)))
        gesture.edges = UIRectEdge.left
        view.addGestureRecognizer(gesture)
    }
    
    @objc func handleGesture(gestureRecognizer: UIScreenEdgePanGestureRecognizer) {
        
        // 1
        let translation = gestureRecognizer.translation(in: gestureRecognizer.view!.superview!)
        var progress = (translation.x / (gestureRecognizer.view!.superview!.frame.size.width))
        progress = CGFloat(fminf(fmaxf(Float(progress), 0.0), 1.0))
        
        switch gestureRecognizer.state {
            
        case .began:
            // 2
            interactionInProgress = true
            viewController.dismiss(animated: true, completion: nil)
            
        case .changed:
            // 3
            shouldCompleteTransition = progress > 0.5
            update(progress)
            
        case .cancelled:
            // 4
            interactionInProgress = false
            cancel()
            
        case .ended:
            // 5
            interactionInProgress = false
            
            if !shouldCompleteTransition {
                cancel()
            } else {
                finish()
            }
            
        default:
            log.debug("Unsupported")
        }
    }
}

extension SwipeInteractionController: UIViewControllerTransitioningDelegate{
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        self.wireToViewController(viewController: presented)
        return PushTransitionAnimation()
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return PopTransitionAnimation()
    }
    
    func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return self.interactionInProgress ? self : nil
    }
}
