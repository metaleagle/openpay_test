//
//  ProfileModuleInteractor.swift
//  OpenPay_test
//
//  Created by Andrew Danishevskyi on 11/5/18.
//  Copyright © 2018 danishevskyi. All rights reserved.
//

import Foundation
class ProfileModuleInteractor{
    let getUserInfoService: GetUserInfoServiceProtocol
    let userProfileStorage: UserProfileStrageServiceProtocol
    init(getUserInfoService: GetUserInfoServiceProtocol, userProfileStorage: UserProfileStrageServiceProtocol){
        self.getUserInfoService = getUserInfoService
        self.userProfileStorage = userProfileStorage
    }
}

extension ProfileModuleInteractor: ProfileModuleInteractorProtocol{
    func initialInit(completion: @escaping () -> Swift.Void){
        self.userProfileStorage.initStorage(completion: completion)
    }
    
    func getUserProfile(completion: @escaping (Profile) -> Swift.Void, failure: @escaping (String) -> Swift.Void){
        if let cachedProfile = self.userProfileStorage.userProfile{
            completion(cachedProfile)
        }
        self.getUserInfoService.loadUserProfile(complition: { (profile) in
            completion(profile)
        }) { (errorMessage) in
            failure(errorMessage)
        }
    }
}
