//
//  SettingsModuleAssembler.swift
//  OpenPay_test
//
//  Created by Andrew Danishevskyi on 11/12/18.
//  Copyright © 2018 danishevskyi. All rights reserved.
//

import UIKit
class SettingsModuleAssembler{
    class func settingsModuleView() -> UIViewController{
        let view = SettingsModuleView()
        let nc = UINavigationController(rootViewController: view)
        nc.navigationBar.barTintColor = UIColor.white
        nc.navigationBar.tintColor = UIColor.black
        nc.navigationBar.prefersLargeTitles = true
        nc.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
        nc.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
        return nc
    }
}
