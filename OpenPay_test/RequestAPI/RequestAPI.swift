//
//  RequestAPI.swift
//
//  Created by Andrew Danishevskyi on 19.02.17.
//  Copyright © 2017 Andrew Danishevskyi. All rights reserved.
//

import Foundation
import UIKit

enum RequestMethod: String{
    case POST = "POST"
    case GET = "GET"
    case PUT = "PUT"
    case DELETE = "DELETE"
}

enum ContentType: String {
    case json = "application/json"
    case none
}

enum ResponseStatus{
    case OK, ERROR, CANCELED
}

protocol ResponseProtocol {
    var responseStatus: ResponseStatus { get }
    func JSONBody() -> [String: Any]?
}

protocol RequestSenderProtocol {
    var baseURL: String { get set }
    var resourcePath: String { get set }
    var method: RequestMethod { get set }
    var requestBodyString: String? { get set }
    var reachabilityManager: ReachabilityManagerProtocol? {set get}
    func sendRequest(withSuccess success: @escaping (ResponseProtocol)-> Swift.Void, errorHandler: @escaping (String) -> Void) -> Void
}

struct URLResponse: ResponseProtocol {
    internal func JSONBody() -> [String : Any]? {
        var json: [String: Any]!
        do {
            json = try JSONSerialization.jsonObject(with: self.dataBytes!, options: []) as? [String: Any]
        } catch {
            return nil
        }
        return json
    }

    internal var responseStatus: ResponseStatus

    var dataBytes: Data?
}

class RequestSender: NSObject, URLSessionDelegate, RequestSenderProtocol{

    var defaultSession: URLSession?
    var dataTask: URLSessionDataTask?
    var baseURL: String
    var resourcePath: String
    var method: RequestMethod
    var contentType: ContentType
    var requestBodyString: String?
    var reachabilityManager: ReachabilityManagerProtocol?
    
    init(baseURL: String, resourcePath: String, method: RequestMethod, requestBodyString: String? = nil, contentType: ContentType = .none, reachabilityManager: ReachabilityManagerProtocol? = nil) {
        self.baseURL = baseURL
        self.resourcePath = resourcePath
        self.method = method
        self.requestBodyString = requestBodyString
        self.contentType = contentType
        self.reachabilityManager = reachabilityManager
        super.init()
        self.defaultSession = URLSession(configuration: .default, delegate: self, delegateQueue: nil)
    }
    
    deinit {
        if dataTask != nil {
            dataTask?.cancel()
        }
    }
    
    func addHeaders(request: inout URLRequest){
        
    }
    
    func sendRequest(withSuccess success: @escaping (ResponseProtocol) -> Void, errorHandler: @escaping (String) -> Void) {
        if let reachabilityManager = self.reachabilityManager{
            if !reachabilityManager.isNetworkAvailable{
                errorHandler("Network connection is absent!")
                return
            }
        }
        if dataTask != nil {
            dataTask?.cancel()
        }
        guard let requestURL = URL(string: "\(baseURL)/\(resourcePath)") else {
            return
        }
        var request = URLRequest(url: requestURL)
        request.httpMethod = self.method.rawValue
        request.httpBody = (nil != self.requestBodyString) ? self.requestBodyString!.data(using: .utf8) : nil
        switch contentType {
        case .none:
            break
        default:
            request.setValue(contentType.rawValue, forHTTPHeaderField: "Content-Type")
        }
        addHeaders(request: &request)
#if DEBUG
        NSLog("Sending %@ request to  %@", self.method.rawValue, requestURL.absoluteString)
        NSLog("Headers: %@", String(describing: request.allHTTPHeaderFields))
        if let body = request.httpBody,
            let bodyString = String(data: body, encoding: .utf8){
            print("request body")
            print(bodyString.stripped(maxParamterLength: 100))
        }
#endif
        dataTask = defaultSession?.dataTask(with: request, completionHandler: { (data, response, error) in
            DispatchQueue.main.async{
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
            }
            
            guard nil == error else {
                let error = error! as NSError
                if error.code == NSURLErrorCancelled {
                    success(URLResponse(responseStatus:.CANCELED, dataBytes:nil))
                    return
                }
                errorHandler(error.localizedDescription)
                return
            }
            if let httpResponse = response as? HTTPURLResponse{
#if DEBUG
                NSLog("  ")
                print("Response received from \(requestURL.absoluteString) with status \(httpResponse.statusCode)")
                if nil != data, let bodyString = String(data: data!, encoding: .utf8){
                    print("response body")
                    print(bodyString)
                }
#endif
                var responseStatus: ResponseStatus
                switch httpResponse.statusCode{
                case (200..<400):
                    responseStatus = .OK
                default:
                    responseStatus = .ERROR
                }
                success(URLResponse(responseStatus:responseStatus, dataBytes:data))
            }
        })
        DispatchQueue.main.async{
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
        dataTask?.resume()
    }
}

extension String{
    func stripped(maxParamterLength: Int) -> String{
        let componenets = self.components(separatedBy: "\"")
        if componenets.count > 1{
            return componenets.reduce(into: "", { (partialResult, nextComponent) in
                let componentRepresentation = (nextComponent.count > maxParamterLength) ? nextComponent.prefix(maxParamterLength / 2) + "..." + nextComponent.suffix(maxParamterLength / 2) : nextComponent
                partialResult += componentRepresentation
            })
        }
        return self
    }
}
