//
//  GetUserInfoServiceProtocol.swift
//  OpenPay_test
//
//  Created by Andrew Danishevskyi on 11/5/18.
//  Copyright © 2018 danishevskyi. All rights reserved.
//

import Foundation
protocol GetUserInfoServiceProtocol{
    func loadUserProfile(complition: @escaping (Profile) -> Swift.Void, failure: @escaping (String) -> Swift.Void)
}
