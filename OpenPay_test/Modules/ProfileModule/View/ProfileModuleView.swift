//
//  ProfileModuleView.swift
//  OpenPay_test
//
//  Created by Andrew Danishevskyi on 11/5/18.
//  Copyright © 2018 danishevskyi. All rights reserved.
//

import UIKit

class ProfileModuleView: UIViewController {
    var presenter: ProfileModuleViewPresenterProtocol?
    
    private var userpicImageView: UIImageView!
    private var usernameLabel: UILabel!
    private var userLocationLabel: UILabel!
    private var logoImageView: UIImageView!
    private var settingsButton: UIButton!
    private var mobileButton: UIButton!
    
    let cardsListView: CardsListViewProtocol
    
    init(cardsListView: CardsListViewProtocol){
        self.cardsListView = cardsListView
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addBackground()
        // Do any additional setup after loading the view.
        self.show(isInitializing: true)
        self.presenter?.initialInit(completion: {[weak self] in
            self?.show(isInitializing: false)
            self?.createInterface()
            self?.presenter?.setup(view: self)
        })
        addCardsListView()
        self.cardsListView.reloadCards()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.cardsListView.updateLayout()
    }
    
    private func addBackground(){
        self.view.backgroundColor = UIColor.white
        let bgImage = UIImageView(frame: self.view.bounds)
        bgImage.image = UIImage(named: "ProfileView_BG")
        self.view.addSubview(bgImage)
        NSLayoutConstraint.activate([
            bgImage.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            bgImage.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
            bgImage.topAnchor.constraint(equalTo: self.view.topAnchor),
            bgImage.bottomAnchor.constraint(equalTo: self.view.bottomAnchor)
            ])
    }
    
    private func addUserpicImageView(){
        self.userpicImageView = UIImageView(frame: CGRect.zero)
        self.userpicImageView.layer.cornerRadius = 52.5
        self.userpicImageView.clipsToBounds = true
        self.userpicImageView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(userpicImageView)
        NSLayoutConstraint.activate([
            userpicImageView.widthAnchor.constraint(equalToConstant: 105),
            userpicImageView.heightAnchor.constraint(equalToConstant: 105),
            userpicImageView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 48.9),
            userpicImageView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -35.1)
            ])
        self.view.layoutIfNeeded()
    }
    
    private func addUsernameLabel(){
        self.usernameLabel = UILabel(frame: CGRect.zero)
        self.usernameLabel.font = UIFont(name: "Poppins-SemiBold", size: 18)
        self.usernameLabel.textColor = #colorLiteral(red: 0.2196078431, green: 0.2745098039, blue: 0.3568627451, alpha: 1)
        self.usernameLabel.translatesAutoresizingMaskIntoConstraints = false
        self.usernameLabel.adjustsFontSizeToFitWidth = false
        self.usernameLabel.lineBreakMode = .byTruncatingTail
        self.usernameLabel.textAlignment = .left
        self.view.addSubview(self.usernameLabel)
        NSLayoutConstraint.activate([
            usernameLabel.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 32.1),
            usernameLabel.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 77),
            usernameLabel.heightAnchor.constraint(equalToConstant: 25),
            usernameLabel.trailingAnchor.constraint(greaterThanOrEqualTo: self.userpicImageView.leadingAnchor, constant: 25)
            ])
        self.view.layoutIfNeeded()
    }
    
    private func addUserLocationLabel(){
        self.userLocationLabel = UILabel(frame: CGRect.zero)
        self.userLocationLabel.font = UIFont(name: "Poppins-Light", size: 14.1)
        self.userLocationLabel.textColor = #colorLiteral(red: 0.2196078431, green: 0.2745098039, blue: 0.3568627451, alpha: 1)
        self.userLocationLabel.translatesAutoresizingMaskIntoConstraints = false
        self.userLocationLabel.adjustsFontSizeToFitWidth = false
        self.userLocationLabel.lineBreakMode = .byTruncatingTail
        self.userLocationLabel.textAlignment = .left
        self.view.addSubview(self.userLocationLabel)
        NSLayoutConstraint.activate([
            userLocationLabel.leadingAnchor.constraint(equalTo: self.usernameLabel.leadingAnchor),
            userLocationLabel.topAnchor.constraint(equalTo:self.usernameLabel.bottomAnchor, constant: 8.1),
            userLocationLabel.heightAnchor.constraint(equalToConstant: 20.1),
            userLocationLabel.trailingAnchor.constraint(equalTo: self.usernameLabel.trailingAnchor)
            ])
        self.view.layoutIfNeeded()
    }
    
    private func addLogo(){
        self.logoImageView = UIImageView(frame: CGRect.zero)
        self.logoImageView.clipsToBounds = true
        self.logoImageView.translatesAutoresizingMaskIntoConstraints = false
        self.logoImageView.image = #imageLiteral(resourceName: "Logo")
        self.view.addSubview(logoImageView)
        NSLayoutConstraint.activate([
            logoImageView.widthAnchor.constraint(equalToConstant: 128),
            logoImageView.heightAnchor.constraint(equalToConstant: 25),
            logoImageView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            logoImageView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -88)
            ])
        self.view.layoutIfNeeded()
    }
    
    private func addButtons(){
        self.settingsButton = UIButton(type: .custom)
        self.settingsButton.setImage(#imageLiteral(resourceName: "settings"), for: .normal)
        self.settingsButton.addTarget(self, action: #selector(settingsAction(_:)), for: .touchUpInside)
        self.settingsButton.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(settingsButton)
        NSLayoutConstraint.activate([
            settingsButton.widthAnchor.constraint(equalToConstant: 30.1),
            settingsButton.heightAnchor.constraint(equalToConstant: 30.1),
            settingsButton.leadingAnchor.constraint(equalTo: self.usernameLabel.leadingAnchor, constant: -5),
            settingsButton.topAnchor.constraint(equalTo: self.userLocationLabel.bottomAnchor, constant: 25)
            ])
        self.mobileButton = UIButton(type: .custom)
        self.mobileButton.setImage(#imageLiteral(resourceName: "mobile"), for: .normal)
        self.mobileButton.addTarget(self, action: #selector(mobileAction(_:)), for: .touchUpInside)
        self.mobileButton.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(mobileButton)
        NSLayoutConstraint.activate([
            mobileButton.widthAnchor.constraint(equalTo: self.settingsButton.widthAnchor),
            mobileButton.heightAnchor.constraint(equalTo: self.settingsButton.heightAnchor),
            mobileButton.leadingAnchor.constraint(equalTo: self.settingsButton.trailingAnchor, constant: 31.9),
            mobileButton.centerYAnchor.constraint(equalTo: self.settingsButton.centerYAnchor)
            ])
        self.view.layoutIfNeeded()
    }
    
    private func addCardsListView(){
        if let cardsListView = cardsListView as? UIView{
            self.view.addSubview(cardsListView)
            NSLayoutConstraint.activate([
                cardsListView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
                cardsListView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
                cardsListView.heightAnchor.constraint(equalToConstant: 228),
                cardsListView.centerYAnchor.constraint(equalTo: self.view.centerYAnchor)
                ])
        }
        
    }
    
    private func show(isInitializing: Bool){
        
    }
    
    private func createInterface(){
        addUserpicImageView()
        addUsernameLabel()
        addUserLocationLabel()
        addLogo()
        addButtons()
    }
    
    @objc func settingsAction(_ sender: UIButton){
        self.presenter?.showSettings(from: self)
    }
    
    @objc func mobileAction(_ sender: UIButton){
         log.debug("mobile action")
    }

}

extension ProfileModuleView: ProfileModuleViewProtocol{
    func showUer(name: String?){
         DispatchQueue.main.async {
            self.usernameLabel.text = name
        }
    }
    
    func showUser(location: String?){
         DispatchQueue.main.async {
            self.userLocationLabel.text = location
        }
    }
    
    func setUserIcon(url: URL?){
         DispatchQueue.main.async {
            self.userpicImageView.setImageURL(url)
        }
    }
}
