//
//  CardsListModulePresenter.swift
//  OpenPay_test
//
//  Created by Andrew Danishevskyi on 11/13/18.
//  Copyright © 2018 danishevskyi. All rights reserved.
//

import Foundation
class CardsListModulePresenter{
    var interactor: CardsListModuleInteractorProtocol?
}

extension CardsListModulePresenter: CardsListModulePresenterProtocol{
    func getUserCards(completion: @escaping()->Swift.Void, failure: @escaping (String)->Swift.Void){
        self.interactor?.getUserCards(completion: completion, failure: failure)
    }
    
    var cardsNumber: Int{
        return self.interactor?.cardsNumber ?? 0
    }
    
    func setup(cardView: UserCardRepresentationViewProtocol, at index: Int){
        if let userCard = self.interactor?.userCard(at: index){
            cardView.setCardImage(url: userCard.imageURL)
        }
    }
}
