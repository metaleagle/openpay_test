//
//  GetUserCardsService.swift
//  OpenPay_test
//
//  Created by Andrew Danishevskyi on 11/13/18.
//  Copyright © 2018 danishevskyi. All rights reserved.
//

import Foundation
class GetUserCardsService{
    fileprivate let requestSender: RequestSenderProtocol
    init(requestSender: RequestSenderProtocol){
        self.requestSender = requestSender
    }
}

extension GetUserCardsService: GetUserCardsServiceProtocol{
    func loadUserCardsList(complition: @escaping ([UserCard]) -> Swift.Void, failure: @escaping (String) -> Swift.Void){
        self.requestSender.sendRequest(withSuccess: { (response) in
            if let cardsList = response.JSONBody()?["cards"] as? [[String: Any]]{
                complition(cardsList.map{UserCard(with: $0)}.compactMap{$0})
            }
            else{
                failure("Unrecognized response")
            }
        }) { (errorMessage) in
            failure(errorMessage)
        }
    }
}
