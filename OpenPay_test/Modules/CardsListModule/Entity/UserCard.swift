//
//  UserCard.swift
//  OpenPay_test
//
//  Created by Andrew Danishevskyi on 11/13/18.
//  Copyright © 2018 danishevskyi. All rights reserved.
//

import Foundation
struct UserCard{
    let imageURLString: String
    let width: Int?
    let height: Int?
    let isDefault: Bool
    
    var imageURL: URL?{
        return URL(string: self.imageURLString)
    }
    
    init(imageURLString: String, width: Int?, height: Int?, isDefault: Bool){
        self.imageURLString = imageURLString
        self.width = width
        self.height = height
        self.isDefault = isDefault
    }
    
    init?(with JSON: [String: Any]?){
        guard let JSON = JSON else {return nil}
        guard let urlString = JSON["imageUrl"] as? String else {return nil}
        guard let isDefault = JSON["isDefault"] as? Bool else {return nil}
        self.init(imageURLString: urlString, width: JSON["width"] as? Int, height: JSON["height"] as? Int, isDefault: isDefault)
    }
}
