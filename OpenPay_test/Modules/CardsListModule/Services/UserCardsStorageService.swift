//
//  UserCardsStorageService.swift
//  OpenPay_test
//
//  Created by Andrew Danishevskyi on 11/13/18.
//  Copyright © 2018 danishevskyi. All rights reserved.
//

import Foundation
class UserCardsStorageService{
    fileprivate var cards: [UserCard]?
}

extension UserCardsStorageService: UserCardsStorageServiceProtocol{
    var userCards: [UserCard]? {
        //extract saved cards
        return cards
    }
    
    func cacheUserCards(_ cards: [UserCard], completion: @escaping () -> Swift.Void){
        //save cards to data storage
        self.cards = cards
        completion()
    }
}
